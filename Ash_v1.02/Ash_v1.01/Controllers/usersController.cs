﻿using Ash_v1._01.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ash_v1._01.Controllers
{
    public class usersController : Controller
    {
        UsersBussiness US = new UsersBussiness();
        public ActionResult Index()
        {
            return View(US.ReturnUsers());
        }
    }
}
