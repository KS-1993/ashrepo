﻿using Ash_v1._01.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ash_v1._01.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private Orders or = new Orders();
        private ProductBussiness PB = new ProductBussiness();
        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Categories = PB.NoRepeate();
            ViewBag.BasketItems = GetUserBasketItems();
            return View(PB.ViewProducts().OrderByDescending(x => x.Id).Take(6));
        }
        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.Categories = PB.NoRepeate();
            ViewBag.BasketItems = GetUserBasketItems();
            return View();
        }
        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            ViewBag.Categories = PB.NoRepeate();
            ViewBag.BasketItems = GetUserBasketItems();
            return View();
        }
        [AllowAnonymous]
        public ActionResult CategoryFilter(string cat)
        {
            ViewBag.Categories = PB.NoRepeate();
            ViewBag.BasketItems = GetUserBasketItems();
            return View(PB.FilterCatagory(cat));
        }
        public ActionResult Details(int? id)
        {
            ViewBag.Categories = PB.NoRepeate();
            ViewBag.BasketItems = GetUserBasketItems();
            return View(PB.FindProduct(id));
        }
        public ActionResult Basket(int Id)
        {
            PB.basket(Id);
            return RedirectToAction("Index");
        }
        public ActionResult RandomSection()
        {
            return View(PB.ViewProducts().Take(3));
        }
        public List<Basket> GetUserBasketItems()
        {
            var Orders = or.GetUserBasketItems();
            return Orders;
        }
    }
}