﻿using System.Web.Mvc;
using PagedList;
using Ash_v1._01.Models;

namespace Ash_v1._01.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductsController : Controller
    {
        private ProductBussiness PB = new ProductBussiness();
        public ActionResult Index(int? i)
        {
            return View(PB.ViewProducts().ToPagedList(i ?? 1,3));
        }
        public ActionResult Details(int? id)
        {
            return View(PB.FindProduct(id));
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Product product)
        {
            PB.AddProduct(product);
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            return View(PB.FindProduct(id));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Product product)
        {
            PB.EditProduct(product);
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? id)
        {
            return View(PB.FindProduct(id));
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = PB.FindProduct(id);
            PB.DeleteProduct(product);
            return RedirectToAction("Index");
        }
    }
}
