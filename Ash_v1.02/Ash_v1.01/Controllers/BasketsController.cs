﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ash_v1._01.Models;

namespace Ash_v1._01.Controllers
{
    public class BasketsController : Controller
    {
        Orders or = new Orders();
        public ActionResult Index()
        {           
            return View(or.GetOrder().ToList());
        }
        public ActionResult Delete(int? id)
        {
            or.DeleteOrder(or.FindOrder(id));
            return RedirectToAction("Index");
        }
    }
}
