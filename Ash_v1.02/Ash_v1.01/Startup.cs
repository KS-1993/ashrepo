﻿using Ash_v1._01.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ash_v1._01.Startup))]
namespace Ash_v1._01
{
    public partial class Startup
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRole();
            CreateUsers();
        }
        public void CreateUsers()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var user = new ApplicationUser();
            user.Email = "Admins@Ash.com";
            user.UserName = "Ashwaq";
            var check = userManager.Create(user, "aassdd93");
            if (check.Succeeded)
            {
                userManager.AddToRole(user.Id, "Admin");
            }
        }
        public void CreateRole()
        {
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            IdentityRole role;
            if (!RoleManager.RoleExists("Admin"))
            {
                role = new IdentityRole();
                role.Name = "Admin";
                RoleManager.Create(role);
            }
            if (!RoleManager.RoleExists("User"))
            {
                role = new IdentityRole();
                role.Name = "User";
                RoleManager.Create(role);
            }
        }
   //     services.AddIdentity<IdentityAppUser, IdentityAppRole>(config =>
			//{
			//	config.Password.RequireDigit = false;
			//	config.Password.RequiredLength = 4;
			//	config.Password.RequireNonAlphanumeric = false;
			//	config.Password.RequireUppercase = false;
			//	config.Password.RequireLowercase = false;
			//	config.Lockout.AllowedForNewUsers = true;
			//	config.Lockout.MaxFailedAccessAttempts = 5;
			//	config.User.RequireUniqueEmail = false;
			//})
    }
}
