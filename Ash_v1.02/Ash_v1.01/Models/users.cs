﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ash_v1._01.Models
{
    public class users
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}