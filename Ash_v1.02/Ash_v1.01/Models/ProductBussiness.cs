﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace Ash_v1._01.Models
{
    public class ProductBussiness
    {
        private CreateBasket cb = new CreateBasket();
        private ApplicationDbContext db = new ApplicationDbContext();
        private UsersBussiness ub = new UsersBussiness();
        public List<Product> ViewProducts()
        {
            return db.Products.ToList();
        }
        public Product Details(int? id)
        {
            Product product = db.Products.Find(id);
            return product;
        }
        public void AddProduct(Product product)
        {
            string fileName = Path.GetFileNameWithoutExtension(product.ImageFile.FileName);
            string extention = Path.GetExtension(product.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extention;
            product.Image = "/Image/" + fileName.Replace("\\", "/");
            
            fileName = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Image/"), fileName.Replace("\\","/"));
            product.ImageFile.SaveAs(fileName);
            
            db.Products.Add(product);
            db.SaveChanges();
        }
        public Product FindProduct(int? Id)
        {
            return db.Products.Where(p => p.Id == Id).FirstOrDefault();
        }
        public void DeleteProduct(Product product)
        {
            db.Products.Remove(product);
            db.SaveChanges();
        }
        public void EditProduct(Product prd)
        {
            var product = db.Products.Where(p => p.Id == prd.Id).FirstOrDefault();
            db.Products.Remove(product);
            db.Products.Add(prd);
            db.SaveChanges();
        }
        public List<string> NoRepeate()
        {
            List<Product> product = new List<Product>();
            List<string> catagory = new List<string>();
            foreach (var item in db.Products)
            {
                if (catagory.Contains(item.Category))
                {
                }
                else
                {
                    catagory.Add(item.Category);
                }
            }
            return catagory;
        }
        public List<Product> FilterCatagory(string cat)
        {
            return db.Products.Where(p=>p.Category == cat).ToList() ;
        }
        public void basket(int Id)
        {
            var userId = HttpContext.Current.User.Identity.GetUserId().ToString();
            var prd = db.Products.Where(p => p.Id == Id).FirstOrDefault();
            var allProducts = db.Products.ToList();
            var users = ub.ReturnUsers();
            var CurrentUser = users.Where(u => u.Id == userId).FirstOrDefault();
            cb.ProcessingBasket(CurrentUser, prd);
        }
    }
}