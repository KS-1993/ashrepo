﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ash_v1._01.Models
{
    public class CreateBasket
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public void ProcessingBasket(ApplicationUser user, Product prd)
        {
            var productId = prd.Id;
            var price = prd.Price;

            db.Basket.Add(
                     new Basket
                     {
                         ProductId = productId,
                         Price = price,
                         Count = 1,
                         user_Id = user.Id
                     });;;
            db.SaveChanges();
           
        }
    }
}