﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ash_v1._01.Models
{
    public class UsersBussiness
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public List<ApplicationUser> ReturnUsers()
        {
            List<ApplicationUser> ListOfUsers = new List<ApplicationUser>();
            foreach (var item in db.Users)
            {
                ListOfUsers.Add(item);
            }
            return ListOfUsers.ToList();
        }
    }
}