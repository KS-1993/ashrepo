﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ash_v1._01.Models;
using Microsoft.AspNet.Identity;

namespace Ash_v1._01.Models
{
    public class Orders
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public List<Basket> GetOrder()
        {
            var basket = db.Basket.Include(b => b.Product);
            return basket.ToList();
        }
        public Basket FindOrder(int? id)
        {
            Basket basket = db.Basket.Find(id);
            return basket;
        }
        public void DeleteOrder(Basket basket)
        {
            db.Basket.Remove(basket);
            db.SaveChanges();
        }
        public List<Basket> GetUserBasketItems()
        {
            if (HttpContext.Current.User.Identity.GetUserId() != null)
            {
                var userId = HttpContext.Current.User.Identity.GetUserId().ToString();
                var basketItems = db.Basket.Include(b => b.Product).Where(b => b.user_Id == userId).ToList();
                return basketItems;
            }
            else
            {
                return new List<Basket>();
            }
        }
    }
}