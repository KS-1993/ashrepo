﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ash_v1._01.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }
        [DataType(DataType.ImageUrl)]
        public string Image { get; set; }
        public string Category { get; set; }
        public double Price { get; set; }
        [DataType(DataType.Text)]
        public string Description { get; set; }
        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }
    }
}